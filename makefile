all:
	GOPATH=$(GOPATH):$(PWD) go get fhash

doc: doc/view.png doc/arch.html

%.html: %.mkd
	markdown $< > $@

%.png: %.dia
	dia --export $@ $<
