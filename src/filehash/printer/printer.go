package printer

import (
    "fmt"
)

type Printable interface {
    String() string
}

func New() chan<- Printable {
    info := make(chan Printable)
    go func() {
        for {
            fmt.Println(<-info)
        }
    }()
    return info
}
