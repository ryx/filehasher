package filehash

import (
    "fmt"
    "hash"
    "time"
)

type Sink interface {
    HashResult(HashInfo)
}

type FileInfo struct {
    Path   string
    Data   []byte
    Error  error
    Hasher hash.Hash
}

func (fi FileInfo) update(data []byte, err error) FileInfo {
    return FileInfo{
        Path:  fi.Path,
        Data:  data,
        Error: err,
    }
}

type HashInfo struct {
    Path  string
    Hash  string
    Error error
}

func (info HashInfo) String() string {
    if info.Error != nil {
        return fmt.Sprintf("error: %s", info.Error)
    }
    return fmt.Sprintf("%s %s", info.Hash, info.Path)
}

// Provides a front-end to TaskPool.
type FileHasher struct {
    Pauseable
    files  FileQueue
    hashes HashQueue
    pool   *TaskPool
    sink   Sink
}

func NewFileHasher(hash string, sink Sink) *FileHasher {
    files := make(FileQueue)
    hashes := make(HashQueue)
    pool := NewTaskPool(hashGen(hash), files, hashes)

    hasher := &FileHasher{
        Pauseable: pool,
        files:     files,
        hashes:    hashes,
        pool:      pool,
        sink: sink,
    }

    go func() {
        for result := range hashes {
            if hasher.sink == nil {
                panic("sink not set")
            }
            hasher.sink.HashResult(result)
        }
    }()

    return hasher
}

// Start hashing a single file.
func (fh *FileHasher) Add(path string, method interface{}) {
    fh.pool.StartHashing(path, method)
}

// Start hashing multiple files.
func (fh *FileHasher) Append(paths []string, hashGen HashGen) {
    for i := range paths {
        var hash hash.Hash
        if hashGen != nil {
            hash = hashGen()
        }
        fh.pool.StartHashing(paths[i], hash)
    }
}

func (fh *FileHasher) SetSink(sink Sink) {
    fh.sink = sink
}

func (fh *FileHasher) Join() {
    fh.Pauseable.Join()
    // wait for the sink
    time.Sleep(time.Millisecond * 10)
}
