package filehash

import (
    "bufio"
    "crypto/sha1"
    "crypto/sha256"
    "crypto/sha512"
    "fmt"
    "hash"
    "io"
    "os"
    "strings"
)

const CHUNK_BYTES = 32000

var ErrTaskKilled = fmt.Errorf("task killed")

// HashGen returns a new hash function with each call.
type HashGen func() hash.Hash

// Return a HashGen by hash name.
func hashGen(s string) HashGen {
    s = strings.ToLower(s)
    switch s {
    case "sha1":
        return func() hash.Hash { return sha1.New() }
    case "sha256":
        return func() hash.Hash { return sha256.New() }
    case "sha512":
        return func() hash.Hash { return sha512.New() }
    }
    panic("unknown hash: " + s)
}

// Converts the name of a hash to a hash.Hash. If method is a hash.Hash, it is
// returned untouched.
func getHasher(method interface{}) (hash.Hash, error) {
    switch value := method.(type) {
    case hash.Hash:
        return value, nil
    case string:
        return hashGen(value)(), nil
    }
    return nil, fmt.Errorf("cannot convert %v to hasher", method)
}

// Open a file as a bufio.Buffer that implements io.Closer.
func open(path string) (io.ReadCloser, error) {
    f, err := os.Open(path)
    if err != nil {
        return nil, err
    }
    return struct {
        io.Reader
        io.Closer
    }{
        bufio.NewReader(f),
        f,
    }, nil
}

// Blocks on SIGINT until a SIGCONT.
// ErrTaskKilled is returned on SIGKILL.
func handleSignal(sig Signal, signals <-chan Signal) error {
    switch sig {
    case SIGINT:
        // wait for a SIGCONT or SIGKILL
        for signal := range signals {
            switch signal {
            case SIGCONT:
                return nil
            case SIGKILL:
                return ErrTaskKilled
            }
        }
    case SIGKILL:
        return ErrTaskKilled
    }
    return nil
}

// Calls f until a SIGKILL or when false is returned from f.
func interruptLoop(signals <-chan Signal, f func() bool) error {
    for {
        select {
        case signal := <-signals:
            if err := handleSignal(signal, signals); err != nil {
                return err
            }
        default:
            if running := f(); !running {
                return nil
            }
        }
    }
    panic("not reached")
}

// Reads a chunk from a bytes.Buffer and writes it to a hash.Hash.
// Returns io.EOF when r is empty.
func copyChunk(r io.Reader, h io.Writer) error {
    var buffer [CHUNK_BYTES]byte
    n, err := r.Read(buffer[:])
    if err != nil {
        return err
    }
    h.Write(buffer[:n])
    return nil
}
