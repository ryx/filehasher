package filehash

import (
    "bytes"
    "fmt"
    "hash"
    "io"
    "runtime"
    "sync"
)

type (
    FileQueue chan FileInfo
    dataQueue chan FileInfo
    HashQueue chan HashInfo
)

type Signal uint

const (
    SIGINT = Signal(iota)
    SIGCONT
    SIGKILL
)

type Signaled interface {
    Signal(Signal)
}

type Pauseable interface {
    Pause()
    Resume()
    Kill()
    Join()
}

// TaskPool reads files and hashes them in separate go routines.
type TaskPool struct {
    files FileQueue
    tasks []Signaled
    group *sync.WaitGroup
}

var _ = Pauseable(new(TaskPool))

func NewTaskPool(h HashGen, files FileQueue, res HashQueue) *TaskPool {
    var tasks []Signaled
    group := &sync.WaitGroup{}
    data := make(dataQueue)

    // read files
    for i := 0; i < runtime.NumCPU(); i++ {
        t := newFileTask(files, data, group)
        tasks = append(tasks, Signaled(t))
    }

    // hash data
    for i := 0; i < runtime.NumCPU(); i++ {
        t := newHashTask(h(), data, res, group)
        tasks = append(tasks, Signaled(t))
    }

    return &TaskPool{
        files: files,
        tasks: tasks,
        group: group,
    }
}

// Start hashing a file.
// method can be a hash.Hash or the name a hash.
func (pool *TaskPool) StartHashing(path string, method interface{}) error {
    h, err := getHasher(method)
    pool.files <- FileInfo{Path: path, Hasher: h}
    return err
}

func (pool *TaskPool) Join() {
    for i := 0; i < 3; i++ {
        pool.group.Wait()
    }
}

func (pool *TaskPool) Pause()  { pool.setSignal(SIGINT) }
func (pool *TaskPool) Resume() { pool.setSignal(SIGCONT) }
func (pool *TaskPool) Kill()   { pool.setSignal(SIGKILL) }

func (pool *TaskPool) setSignal(sig Signal) {
    for _, task := range pool.tasks {
        task.Signal(sig)
    }
}

type task struct {
    signals chan Signal
    group   *sync.WaitGroup
}

func newFileTask(files FileQueue, data dataQueue, m *sync.WaitGroup) Signaled {
    return (&task{
        signals: make(chan Signal),
        group:   m,
    }).startReading(files, data)
}

type hashTask struct {
    task
    hash hash.Hash
}

func newHashTask(h hash.Hash, data dataQueue, res HashQueue, m *sync.WaitGroup) Signaled {
    return (&hashTask{
        task: task{
            signals: make(chan Signal),
            group:   m,
        },
        hash: h,
    }).startHashing(data, res)
}

func (t *task) Signal(sig Signal) {
    t.signals <- sig
}

func (t *task) startReading(files FileQueue, data dataQueue) Signaled {
    go func() {
        for {
            // FIXME blocks without handling signals
            info := <-files
            t.group.Add(1)

            r, err := open(info.Path)
            if err != nil {
                data <- info.update(nil, err)
                t.group.Done()
                continue
            }

            buffer := new(bytes.Buffer)
            err = interruptLoop(t.signals, func() bool {
                if err := copyChunk(r, buffer); err != nil {
                    if err == io.EOF {
                        err = nil
                    }
                    data <- info.update(buffer.Bytes(), err)
                    return false
                }
                return true
            })

            r.Close()
            t.group.Done()

            if err == ErrTaskKilled {
                return
            }
        }
    }()

    return t
}

func (t *hashTask) startHashing(data dataQueue, res HashQueue) Signaled {
    go func() {
        for {
            data := <-data
            t.group.Add(1)

            if data.Error != nil {
                res <- HashInfo{
                    Path:  data.Path,
                    Error: data.Error,
                }

                t.group.Done()
                continue
            }

            r := bytes.NewBuffer(data.Data)

            hasher := data.Hasher
            if hasher == nil {
                hasher = t.hash
            }

            hasher.Reset()

            err := interruptLoop(t.signals, func() bool {
                if err := copyChunk(r, hasher); err != nil {
                    if err == io.EOF {
                        err = nil
                    }
                    res <- HashInfo{
                        Path:  data.Path,
                        Hash:  fmt.Sprintf("%x", hasher.Sum(nil)),
                        Error: err,
                    }
                    return false
                }
                return true
            })

            t.group.Done()

            if err == ErrTaskKilled {
                return
            }
        }
    }()

    return t
}
