package main

import (
    "filehash"
    "flag"
    "fmt"
    "os"
)

var method = flag.String("m", "sha1", "sha1, sha256 or sha512")

func main() {
    hasher := filehash.NewFileHasher(*method, Printer(0))
    hasher.Append(files(), nil)
    hasher.Join()
}

func files() []string {
    flag.Parse()
    files := flag.Args()
    if len(files) == 0 {
        fmt.Println("use: filehash [-m hash] [filepath ...]")
        os.Exit(1)
    }
    return files
}

type Printer int

func (p Printer) HashResult(info filehash.HashInfo) {
    println(info.String())
}
